//
//  MVVMViewModel.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import Foundation

protocol MVVMViewModelDelegate {
    var text: String { get set }
    var name: String { get set }
    var textDidChangedHandler: ((String) -> Void)? { get set }
    var nameDidChangeHandler: ((String) -> Void)? { get set }
    
    func obtainNumbers()
    func changeName()
}

class MVVMViewModel: MVVMViewModelDelegate {
    var dataManager: DataManagerProtocol!
    var textDidChangedHandler: ((String) -> Void)?
    var nameDidChangeHandler: ((String) -> Void)?
    
    var text: String = "" {
        didSet {
            textDidChangedHandler?(text)
        }
    }
    
    var name: String = "" {
        didSet {
            nameDidChangeHandler?(name)
        }
    }
    
    func obtainNumbers() {
        let nums = dataManager.obtainNumbers()
        
        text = nums.map { "\($0)" }.joined(separator: ",")
    }
    
    func changeName() {
        let newName = dataManager.getName()
        
        name = newName
    }
}
