//
//  MVVMAssembly.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import UIKit

class MVVMAssembly: NSObject {
    @IBOutlet weak var vc: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        guard let view = vc as? MVVMViewController else { return }
        let viewModel = MVVMViewModel()
        let dataManager = DataManager()
        
        view.viewModel = viewModel
        viewModel.dataManager = dataManager
    }
}
