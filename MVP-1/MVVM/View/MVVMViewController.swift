//
//  MVVMViewController.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import UIKit

class MVVMViewController: UIViewController {
    @IBOutlet weak var labelView: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var viewModel: MVVMViewModelDelegate? {
        didSet {
            viewModel?.textDidChangedHandler = { [unowned self] text in
                self.labelView.text = text
            }
            viewModel?.nameDidChangeHandler = { [unowned self] newName in
                self.nameLabel.text = newName
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func handleBtn() {
        viewModel?.obtainNumbers()
    }
    
    @IBAction func handleChangeName() {
        viewModel?.changeName()
    }
}
