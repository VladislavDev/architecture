//
//  DataManager.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import Foundation

protocol DataManagerProtocol {
    func obtainNumbers() -> [Int]
    func getName() -> String
}

class DataManager: DataManagerProtocol {
    func obtainNumbers() -> [Int] {
        return [1,2,3,4,5,6]
    }
    
    func getName() -> String {
        return "Mike \(UUID().uuidString)"
    }
}
