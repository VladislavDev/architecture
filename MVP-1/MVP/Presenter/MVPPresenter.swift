//
//  MVPPresenter.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import Foundation

// Связь с view
protocol MVPViewInputProtocol: class {
    func showNumbers(_ numbers: [Int])
}

// Связь с presenter
protocol MVPViewOutputProtocol {
    func didPressedAction()
}

class MVPPresenter: MVPViewOutputProtocol {
    weak var vc: MVPViewInputProtocol!
    var dataManager: DataManagerProtocol!
    
    func didPressedAction() {
        let nums = dataManager.obtainNumbers()
        
        vc.showNumbers(nums)
    }
    
    
}
