
import UIKit

class MVPAssembly: NSObject {
    @IBOutlet weak var vc: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let dataManager = DataManager()
        guard let view = vc as? MVPViewController else { return }
        let presenter = MVPPresenter()
        
        view.output = presenter
        presenter.vc = view
        presenter.dataManager = dataManager
    }
}
