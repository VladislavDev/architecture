//
//  MVPViewController.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import UIKit

class MVPViewController: UIViewController, MVPViewInputProtocol {
    var output: MVPViewOutputProtocol!
    
    @IBOutlet weak var labelView: UILabel!
    
    // MARK: Input
    func showNumbers(_ numbers: [Int]) {
        let numsStr = numbers.map { "\($0)" }.joined(separator: ",")
        
        labelView.text = numsStr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    // MARK: Actions
    @IBAction func btnAction() {
        output.didPressedAction()
    }
    
}

