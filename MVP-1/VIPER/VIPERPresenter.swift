//
//  VIPERPresenter.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import Foundation

class VIPERPresenter: VIPERViewOutputProtocol, VIPERInteractorOutputProtocol {
    weak var view: VIPERViewInputProtocol!
    var interactor: VIPERInteractorInputProtocol!
    var router: VIPERRouterInputProtocol!

    // MARK: ViewOutput
    func didPressedAction() {
        interactor.obtainFormattedStr()
    }
    
    // MARK: InteractorOutput
    func didFinishObtainingFormattedStr(_ str: String) {
        view.showFormattedStr(str)
        router.showAlert()
    }
}
