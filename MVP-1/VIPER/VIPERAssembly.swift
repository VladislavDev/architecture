//
//  VIPERAssembly.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import UIKit

class VIPERAssembly: NSObject {
    @IBOutlet weak var vc: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let dataManager = DataManager()
        guard let view = vc as? VIPERViewController else { return }
        let presenter = VIPERPresenter()
        let interactor = VIPERInteractor()
        let router = VIPERRouter()
        
        view.output = presenter
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        interactor.dataManager = dataManager
        interactor.output = presenter
        
        router.view = view
    }
}
