//
//  VIPERViewController.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import UIKit

protocol VIPERViewInputProtocol: class {
    func showFormattedStr(_ str: String)
}

protocol VIPERViewOutputProtocol: class {
    func didPressedAction()
}

class VIPERViewController: UIViewController, VIPERViewInputProtocol {
    @IBOutlet weak var textLabel: UILabel!
    
    var output: VIPERViewOutputProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func showFormattedStr(_ str: String) {        
        textLabel.text = str
    }
    
    @IBAction func handleBtn() {
        output.didPressedAction()
    }
}
