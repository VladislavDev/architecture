//
//  VIPERInteractor.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import Foundation

protocol VIPERInteractorInputProtocol: class {
    func obtainFormattedStr()
}

protocol VIPERInteractorOutputProtocol: class {
    func didFinishObtainingFormattedStr(_ str: String)
}

class VIPERInteractor: VIPERInteractorInputProtocol {
    var output: VIPERInteractorOutputProtocol!
    var dataManager: DataManagerProtocol!
    
    func obtainFormattedStr() {
        let nums = dataManager.obtainNumbers()
        let numsStr = nums.map { "\($0)" }.joined(separator: ",")
        
        output.didFinishObtainingFormattedStr(numsStr)
    }
}
