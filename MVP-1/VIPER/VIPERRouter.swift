//
//  VIPERRouter.swift
//  MVP-1
//
//  Created by Vlad on 11/6/20.
//

import UIKit

protocol VIPERRouterInputProtocol: class {
    func showAlert()
}

class VIPERRouter: VIPERRouterInputProtocol {
    weak var view: UIViewController!
    
    func showAlert() {
        let alert = UIAlertController(title: "Lala", message: "Lala", preferredStyle: .alert)
        let btn = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alert.addAction(btn)
        
        view.present(alert, animated: true, completion: nil)
    }
}
